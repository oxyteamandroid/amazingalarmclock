/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.database;

public class AlarmClockValue {
    private int mMinutes;
    private int mDay;
    private String mListMinutes;
    private String mListDay;
    private int mType;
    private boolean mEnable;

    public AlarmClockValue(String listminutes, String listday, int type,
            boolean enable) {
        super();
        this.mListMinutes = listminutes;
        this.mListDay = listday;
        this.mType = type;
        this.mEnable = enable;
    }

    public AlarmClockValue(int minutes, int day, int type, boolean enable) {
        super();
        this.mMinutes = minutes;
        this.mDay = day;
        this.mType = type;
        this.mEnable = enable;
    }

    public boolean isEnable() {
        return mEnable;
    }

    public void setEnable(boolean enable) {
        this.mEnable = enable;
    }

    public int getMinutes() {
        return mMinutes;
    }

    public void setMinutes(int minutes) {
        this.mMinutes = minutes;
    }

    public int getDay() {
        return mDay;
    }

    public void setDay(int day) {
        this.mDay = day;
    }

    public String getListminutes() {
        return mListMinutes;
    }

    public void setListminutes(String listminutes) {
        this.mListMinutes = listminutes;
    }

    public String getListday() {
        return mListDay;
    }

    public void setListday(String listday) {
        this.mListDay = listday;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

}
