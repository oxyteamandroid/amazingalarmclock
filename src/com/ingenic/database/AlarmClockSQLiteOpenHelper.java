/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.database;

import com.ingenic.utils.CommonUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlarmClockSQLiteOpenHelper extends SQLiteOpenHelper {

    // version
    private final static int VERSION = 1;
    // database name
    private static final String DATABASE_NAME = "alarmclock";

    // create table sql sentences
    private static final String CREATE_TABLE_ALARM_CLOCK = "CREATE TABLE IF NOT EXISTS "
            + CommonUtils.TABLE_ALARM_CLOCK + " (" + CommonUtils.CONTENT_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CommonUtils.CONTENT_COLUMN_MINUTES
            + " INTEGER NOT NULL , " + CommonUtils.CONTENT_COLUMN_DAY
            + " INTEGER NOT NULL, " + CommonUtils.CONTENT_COLUMN_ENABLE
            + " INTEGER NOT NULL)";

    private static final String CREATE_TABLE_ALARM_LIST = "CREATE TABLE IF NOT EXISTS "
            + CommonUtils.TABLE_ALARM_LIST + " (" + CommonUtils.CONTENT_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CommonUtils.CONTENT_COLUMN_LISTMINUTE + " TEXT NOT NULL , "
            + CommonUtils.CONTENT_COLUMN_LISTDAY + " TEXT NOT NULL,"
            + CommonUtils.CONTENT_COLUMN_ENABLE + " INTEGER NOT NULL)";

    private static AlarmClockSQLiteOpenHelper Instance = null;

    public synchronized static AlarmClockSQLiteOpenHelper getInstance(
            Context context) {
        if (Instance == null) {
            Instance = new AlarmClockSQLiteOpenHelper(context);
        }
        return Instance;
    }

    public AlarmClockSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table
        db.execSQL(CREATE_TABLE_ALARM_CLOCK);
        db.execSQL(CREATE_TABLE_ALARM_LIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // update table
        db.execSQL("drop table if exists " + CommonUtils.TABLE_ALARM_CLOCK + ";");
        db.execSQL("drop table if exists " + CommonUtils.TABLE_ALARM_LIST + ";");

        onCreate(db);
    }

}
