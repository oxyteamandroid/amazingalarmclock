/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.database;

import java.util.Calendar;
import java.util.List;

import com.ingenic.alarmclock.MainActivity;
import com.ingenic.utils.CommonUtils;
import com.ingenic.utils.TimeDay;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AlarmClockDbOperation {

    private AlarmClockSQLiteOpenHelper mSqLiteOpenHelper;
    private SQLiteDatabase mSqLiteDatabase = null;
    private Cursor mCursor = null;
    private Calendar mCalendar = null;


    public AlarmClockDbOperation(Context context) {
        mSqLiteOpenHelper = AlarmClockSQLiteOpenHelper.getInstance(context);
    }

    public synchronized void insert(AlarmClockValue value) {

        ContentValues content = new ContentValues();
        // get the database write object
        mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();
        if (value.getType() == 1) {
            // table alarmclock
            content.put("minutes", value.getMinutes());
            content.put("day", value.getDay());
            content.put("enable", getInt(value.isEnable()));
            // do insert
            mSqLiteDatabase.insert(CommonUtils.TABLE_ALARM_CLOCK, null, content);
        } else if (value.getType() == 2) {
            // table alarmlist
            content.put("listminute", value.getListminutes());
            content.put("listday", value.getListday());
            content.put("enable", getInt(value.isEnable()));
            // do insert
            mSqLiteDatabase.insert(CommonUtils.TABLE_ALARM_LIST, null, content);
        }
        content = null;
    }

    // delete all record in table,this application didn't need
    public synchronized void deleteTableRecord(String name) {
        // get write object
        mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();
        // delete all records
        mSqLiteDatabase.delete(name, null, null);
    }

    // table alarmlist,for activity ListView reveal
    public synchronized void querySecond(List<TimeDay> timeDayLists) {
        // get read object
        mSqLiteDatabase = mSqLiteOpenHelper.getReadableDatabase();
        mCursor = mSqLiteDatabase.rawQuery("select * from " + CommonUtils.TABLE_ALARM_LIST
                + " order by id desc", null);
        if (mCursor.getCount() != 0) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                timeDayLists.add(new TimeDay(mCursor.getString(mCursor
                        .getColumnIndex(CommonUtils.CONTENT_COLUMN_LISTMINUTE)), mCursor
                        .getString(mCursor
                                .getColumnIndex(CommonUtils.CONTENT_COLUMN_LISTDAY)),
                        getBoolean(mCursor.getInt(mCursor
                                .getColumnIndex(CommonUtils.CONTENT_COLUMN_ENABLE)))));
                mCursor.moveToNext();
            }

        }

        // 关闭游标
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }

    }

    public synchronized int querySecondCount() {
        int count = 0;
        // get read object
        mSqLiteDatabase = mSqLiteOpenHelper.getReadableDatabase();
        mCursor = mSqLiteDatabase.rawQuery("select * from " + CommonUtils.TABLE_ALARM_LIST,
                null);
        count = mCursor.getCount();
        // 关闭游标
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }

        return count;

    }

    // int transfer to boolean
    private boolean getBoolean(int enable) {
        return (enable == 1) ? true : false;
    }

    // boolean transfer to int
    private int getInt(boolean enable) {
        return enable ? 1 : 0;
    }

    // just for tablelist
    public synchronized String querySecondState(AlarmClockValue value) {

        // get read object
        mSqLiteDatabase = mSqLiteOpenHelper.getReadableDatabase();
        mCursor = mSqLiteDatabase.rawQuery(
                "select " + CommonUtils.CONTENT_COLUMN_LISTDAY + " from "
                        + CommonUtils.TABLE_ALARM_LIST + " where "
                        + CommonUtils.CONTENT_COLUMN_LISTMINUTE + " = '"
                        + value.getListminutes() + "' and "
                        + CommonUtils.CONTENT_COLUMN_ENABLE + " = "
                        + getInt(value.isEnable()) + " and "
                        + CommonUtils.CONTENT_COLUMN_LISTDAY + " != " + "'"
                        + value.getListday() + "'", null);
        String tempString = CommonUtils.EMPTY_STRING;
        String resultString = "";
        if (mCursor.getCount() != 0) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                for (int i = 0; i < 8; i++) {
                    if (mCursor.getString(0).charAt(i) == CommonUtils.REPEAT
                            || tempString.charAt(i) ==CommonUtils.REPEAT) {
                        resultString += CommonUtils.REPEAT;
                    } else {
                        resultString += CommonUtils.NOT_REPEAT;
                    }
                }
                tempString = resultString;
                resultString = "";

                mCursor.moveToNext();
            }
        }

        // 关闭游标
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }
        return tempString;
    }

    /**
     *  query the new clock seconds
     * @return
     */
    public synchronized int[] queryFirstSeconds() {
		int seconds = -1;
		int repeatDay = -1;
		int minutes = -1;
		mSqLiteDatabase = mSqLiteOpenHelper.getReadableDatabase();
		initCalendar();
		int systemMinutes = mCalendar.get(Calendar.HOUR_OF_DAY) * 60
				+ mCalendar.get(Calendar.MINUTE);
		int systemSeconds = systemMinutes * 60 + mCalendar.get(Calendar.SECOND);
		int day = mCalendar.get(Calendar.DAY_OF_WEEK) - 1;
		// 搜索今天比当前晚的时间
		mCursor = mSqLiteDatabase.rawQuery("select "
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " , "
				+ CommonUtils.CONTENT_COLUMN_ID + " , "
				+ CommonUtils.CONTENT_COLUMN_DAY + " from "
				+ CommonUtils.TABLE_ALARM_CLOCK + " where "
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " > " + systemMinutes
				+ " and " + CommonUtils.CONTENT_COLUMN_DAY + " = " + day
				+ " and " + CommonUtils.CONTENT_COLUMN_ENABLE + " = 1"
				+ " order by " + CommonUtils.CONTENT_COLUMN_MINUTES + " asc",
				null);

		if (mCursor.getCount() != 0) {
			mCursor.moveToFirst();
			minutes = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES));
			seconds = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES))
					* 60
					- systemSeconds;
			repeatDay = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY));
		} else {
			// 今天没有比当前晚的时间 则搜索一次性的闹钟(day=7)
			mCursor = mSqLiteDatabase.rawQuery("select "
					+ CommonUtils.CONTENT_COLUMN_MINUTES + " , "
					+ CommonUtils.CONTENT_COLUMN_ID + " , "
					+ CommonUtils.CONTENT_COLUMN_DAY + " from "
					+ CommonUtils.TABLE_ALARM_CLOCK + " where "
					+ CommonUtils.CONTENT_COLUMN_MINUTES + " > "
					+ systemMinutes + " and " + CommonUtils.CONTENT_COLUMN_DAY
					+ " = " + 7 + " and " + CommonUtils.CONTENT_COLUMN_ENABLE
					+ " = 1" + " order by "
					+ CommonUtils.CONTENT_COLUMN_MINUTES + " asc", null);
			if (mCursor.getCount() != 0) {
				mCursor.moveToFirst();
				minutes = mCursor.getInt(mCursor
						.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES));
				seconds = mCursor.getInt(mCursor
						.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES))
						* 60 - systemSeconds;
				repeatDay = mCursor.getInt(mCursor
						.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY));
			}

			// 关闭游标
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (seconds != -1) {
				int[] data = { seconds, minutes, repeatDay };
				return data;
			}
		}

		// 关闭游标
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
		// 搜索一次性闹钟时间比今天最近闹钟时间更早的
		mCursor = mSqLiteDatabase.rawQuery("select "
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " , "
				+ CommonUtils.CONTENT_COLUMN_ID + " , "
				+ CommonUtils.CONTENT_COLUMN_DAY + " from "
				+ CommonUtils.TABLE_ALARM_CLOCK + " where "
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " > " + systemMinutes
				+ " and " + CommonUtils.CONTENT_COLUMN_MINUTES + " < "
				+ minutes + " and " + CommonUtils.CONTENT_COLUMN_DAY + " = "
				+ 7 + " and " + CommonUtils.CONTENT_COLUMN_ENABLE + " = 1"
				+ " order by " + CommonUtils.CONTENT_COLUMN_MINUTES + " asc",
				null);
		if (mCursor.getCount() != 0) {
			mCursor.moveToFirst();
			minutes = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES));
			seconds = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES))
					* 60
					- systemSeconds;
			repeatDay = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY));
		}
		// 关闭游标
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
		if (seconds != -1) {
			int[] data = { seconds, minutes, repeatDay };
			return data;
		}

		// 搜索其他天比现在晚的时间
		mCursor = mSqLiteDatabase.rawQuery("select * from "
				+ CommonUtils.TABLE_ALARM_CLOCK + " where "
				+ CommonUtils.CONTENT_COLUMN_DAY + " > " + day + " and "
				+ CommonUtils.CONTENT_COLUMN_ENABLE + " = 1" + " order by "
				+ CommonUtils.CONTENT_COLUMN_DAY + ","
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " asc", null);

		if (mCursor.getCount() != 0) {
			mCursor.moveToFirst();
			seconds = (mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY)) - day)
					* 24
					* 60
					* 60
					+ mCursor
							.getInt(mCursor
									.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES))
					* 60 - systemSeconds;
			minutes = (mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY)) - day)
					* 24
					* 60
					+ mCursor
							.getInt(mCursor
									.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES));
			repeatDay = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY));

		}

		// 关闭游标
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}

		if (seconds != -1) {
			int[] data = { seconds, minutes, repeatDay };
			return data;
		}

		// 搜索其他天比现在早的时间
		mCursor = mSqLiteDatabase.rawQuery("select * from "
				+ CommonUtils.TABLE_ALARM_CLOCK + " where "
				+ CommonUtils.CONTENT_COLUMN_DAY + " <= " + day + " and "
				+ CommonUtils.CONTENT_COLUMN_ENABLE + " = 1" + " order by "
				+ CommonUtils.CONTENT_COLUMN_DAY + ","
				+ CommonUtils.CONTENT_COLUMN_MINUTES + " asc", null);

		if (mCursor.getCount() != 0) {
			mCursor.moveToFirst();
			seconds = (mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY)) + 7 - day)
					* 24
					* 60
					* 60
					+ mCursor
							.getInt(mCursor
									.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES))
					* 60 - systemSeconds;
			minutes = (mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY)) + 7 - day)
					* 24
					* 60
					+ mCursor
							.getInt(mCursor
									.getColumnIndex(CommonUtils.CONTENT_COLUMN_MINUTES));
			repeatDay = mCursor.getInt(mCursor
					.getColumnIndex(CommonUtils.CONTENT_COLUMN_DAY));
		}

		// 关闭游标
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}

		if (seconds != -1) {
			int[] data = { seconds, minutes, repeatDay };
			return data;
		}
		int[] data = { seconds, minutes, repeatDay };
		return data;
    }

    // 查询记录是否重复
    public synchronized boolean queryRepeat(AlarmClockValue value) {
        boolean isRepeat = false;
        // 获取数据库读对象
        mSqLiteDatabase = mSqLiteOpenHelper.getReadableDatabase();
        if (value.getType() == 1) {
            mCursor = mSqLiteDatabase.rawQuery(
                    "select * from " + CommonUtils.TABLE_ALARM_CLOCK + " where "
                            + CommonUtils.CONTENT_COLUMN_MINUTES + " = "
                            + value.getMinutes() + " and " + CommonUtils.CONTENT_COLUMN_DAY
                            + " = " + value.getDay(), null);
            if (mCursor.getCount() != 0) {
                // 有数据意味着重复，置为true
                mCursor.moveToFirst();
                isRepeat = true;
            }
        } else if (value.getType() == 2) {
            mCursor = mSqLiteDatabase.rawQuery("select * from " + CommonUtils.TABLE_ALARM_LIST
                    + " where " + CommonUtils.CONTENT_COLUMN_LISTMINUTE + " = " + "'"
                    + value.getListminutes() + "'" + " and "
                    + CommonUtils.CONTENT_COLUMN_LISTDAY + " = " + "'" + value.getListday()
                    + "'", null);
            if (mCursor.getCount() != 0) {
                // have data means repeat, set true
                mCursor.moveToFirst();
                isRepeat = true;
            }
        }

        // 关闭游标
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }

        return isRepeat;
    }

    // update
    public synchronized void update(AlarmClockValue value) {
        // construct data
        ContentValues content = new ContentValues();
        content.put("enable", getInt(value.isEnable()));
        // get write object
        mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();

		if (value.getType() == 1) {
			mSqLiteDatabase.update(CommonUtils.TABLE_ALARM_CLOCK, content,
					CommonUtils.CONTENT_COLUMN_MINUTES + " = ? and "
							+ CommonUtils.CONTENT_COLUMN_DAY + " = ? ",
					new String[] { Integer.toString(value.getMinutes()),
							Integer.toString(value.getDay()) });
		} else if (value.getType() == 2) {
			mSqLiteDatabase
					.update(CommonUtils.TABLE_ALARM_LIST,
							content,
							CommonUtils.CONTENT_COLUMN_LISTMINUTE + " = ? and "
									+ CommonUtils.CONTENT_COLUMN_LISTDAY
									+ " = ? ",
							new String[] { value.getListminutes(),
									value.getListday() });
		}

    }

	// 一次性闹钟调用的删除
	public synchronized void delete(int minutes, int repeatDay) {
		// get write object
		mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();
		mSqLiteDatabase.delete(
				CommonUtils.TABLE_ALARM_CLOCK,
				CommonUtils.CONTENT_COLUMN_MINUTES + " = ? and "
						+ CommonUtils.CONTENT_COLUMN_DAY + " = ? ",
				new String[] { Integer.toString(minutes),
						Integer.toString(repeatDay) });
		mSqLiteDatabase
				.delete(CommonUtils.TABLE_ALARM_LIST,
						CommonUtils.CONTENT_COLUMN_LISTMINUTE + " =? and "
								+ CommonUtils.CONTENT_COLUMN_LISTDAY + "=? ",
						new String[] { CommonUtils.transferMinutes(minutes),
								"00000001" });
	}
    // data delete operate
    public synchronized void delete(AlarmClockValue value) {

        // get write object
        mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();

        if (value.getType() == 1) {
            mSqLiteDatabase.delete(CommonUtils.TABLE_ALARM_CLOCK, CommonUtils.CONTENT_COLUMN_MINUTES
                    + " = ? and " + CommonUtils.CONTENT_COLUMN_DAY + " = ? ",
                    new String[] { Integer.toString(value.getMinutes()),
                            Integer.toString(value.getDay()) });
        } else if (value.getType() == 2) {
            mSqLiteDatabase
                    .delete(CommonUtils.TABLE_ALARM_LIST,
							CommonUtils.CONTENT_COLUMN_LISTMINUTE + " =? and "
                                    + CommonUtils.CONTENT_COLUMN_LISTDAY + "=? ",
                            new String[] { value.getListminutes(),
                                    value.getListday() });
        }
    }

    private void initCalendar() {
        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }
        mCalendar.setTimeInMillis(System.currentTimeMillis());
    }
}
