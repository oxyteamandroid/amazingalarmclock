package com.ingenic.utils;

public class CommonUtils {

	// table name
	public static final String TABLE_ALARM_CLOCK = "alarmclock";
	public static final String TABLE_ALARM_LIST = "alarmlist";
	// field
	public static final String CONTENT_COLUMN_ID = "id";
	public static final String CONTENT_COLUMN_LISTMINUTE = "listminute";
	public static final String CONTENT_COLUMN_LISTDAY = "listday";
	public static final String CONTENT_COLUMN_ENABLE = "enable";
	public static final String CONTENT_COLUMN_MINUTES = "minutes";
	public static final String CONTENT_COLUMN_DAY = "day";

	public static final String ACTION_NOTICE = "com.ingenic.amazingalarmclock.notice";

	// 一次性闹钟响后删除后 通知主界面重新加载数据
	public static final String ACTION_UPDATE_LISTVIEW = "com.ingenic.clock.updatelistview";

	// ClockReceiver 与 ClockInfoActivity 之间传递的数据
	public static final String BUNDLE_MINUTES = "minutes";
	public static final String BUNDLE_REPEATDAY = "repeatDay";

	/**
	 * NumberPickerActivity 与 TimeDayActivity 之间传递的数据 HOUR_KEY 小时 MINUTE_KEY 分钟
	 */
	public static final String HOUR_KEY = "hour";
	public static final String MINUTE_KEY = "minute";

	/**
	 * 一周 加一次性 7+1
	 */
	public static final int DAY_WEEK = 8;

	public static final int HOUR_TO_SECOND = 60;
	/**
	 * 是否重复一周中某一天 1 重复 0不重复
	 */
	public static final char REPEAT = '1';
	public static final char NOT_REPEAT = '0';

	/**
	 * 表示一周七天内那几天是重复闹钟的 最后一位代表：一次性闹钟
	 */
	public static final String EMPTY_STRING = "00000000";

	/**
	 * ->00:00
	 */
	public static String transferMinutes(int minutes) {
		return format(minutes / 60) + ":" + format(minutes % 60);
	}

	/**
	 * 0~9 -> 00~09
	 */
	public static String format(int value) {
		String tmpStr = String.valueOf(value);
		if (value < 10) {
			tmpStr = "0" + tmpStr;
		}
		return tmpStr;
	}
}
