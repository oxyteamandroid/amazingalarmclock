package com.ingenic.utils;

import com.ingenic.alarmclock.R;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

/**
 * 自定义数字选择器 NumberPicker 改变字体 文字大小 颜色
 */
public class AlarmClockNumberPicker extends NumberPicker {
	private Typeface mTf;

	public AlarmClockNumberPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		mTf = Typeface
				.createFromAsset(context.getAssets(), "fonts/fzlthjw.ttf");
	}

	@Override
	public void addView(View child) {
		super.addView(child);
		updateView(child);
	}

	@Override
	public void addView(View child, int index,
			android.view.ViewGroup.LayoutParams params) {
		super.addView(child, index, params);
		updateView(child);
	}

	@Override
	public void addView(View child, android.view.ViewGroup.LayoutParams params) {
		super.addView(child, params);
		updateView(child);
	}

	public void updateView(View view) {
		if (view instanceof EditText) {
			// 修改字体的属性
			((EditText) view).setTextSize(getResources().getDimension(
					R.dimen.numberPickerSize));
			((EditText) view).setTextColor(getResources().getColor(
					R.color.purple));
			((EditText) view).setTypeface(mTf);
		}
	}
}
