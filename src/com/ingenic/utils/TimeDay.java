/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.utils;

public class TimeDay {
    private String mTimes;
    private String mDays;
    private boolean mEnable;

    @Override
    public String toString() {
        return "times:" + mTimes + ",days:" + mDays + ",enable:" + mEnable;
    }

    public TimeDay(String times, String days, boolean enable) {
        super();
        this.mTimes = times;
        this.mDays = days;
        this.mEnable = enable;
    }

    public boolean isEnable() {
        return mEnable;
    }

    public void setEnable(boolean enable) {
        this.mEnable = enable;
    }

    public void setTimes(String times) {
        this.mTimes = times;
    }

    public void setDays(String days) {
        this.mDays = days;
    }

    public String getTimes() {
        return mTimes;
    }

    public String getDays() {
        return mDays;
    }

}
