/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

import java.io.IOException;

public class RingInsist {
    static MediaPlayer MediaPlayer = null;

    public static void startRing(Context context)
            throws IllegalArgumentException, SecurityException,
            IllegalStateException, IOException {
        // 获取alarm uri
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        // 创建media player
        MediaPlayer = new MediaPlayer();
        MediaPlayer.setDataSource(context, alert);
        final AudioManager audioManager = (AudioManager) context
                .getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
            MediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            MediaPlayer.setLooping(true);
            MediaPlayer.prepare();
            MediaPlayer.start();
        }

    }

    public static void stopRing() {
        if (MediaPlayer != null) {
            MediaPlayer.stop();
            MediaPlayer.release();
            MediaPlayer = null;
        }
    }
}
