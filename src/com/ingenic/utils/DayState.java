/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.utils;

import android.widget.Button;

public class DayState {
    private Button mButton;
    private boolean mIsSecond;

    public DayState(Button button, boolean isSecond) {
        super();
        this.mButton = button;
        this.mIsSecond = isSecond;
    }

    public void setButton(Button button) {
        this.mButton = button;
    }

    public void setSecond(boolean isSecond) {
        this.mIsSecond = isSecond;
    }

    public Button getButton() {
        return mButton;
    }

    public boolean isSecond() {
        return mIsSecond;
    }

}
