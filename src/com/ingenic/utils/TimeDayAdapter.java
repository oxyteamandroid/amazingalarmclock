/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.utils;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.alarmclock.ClockReceiver;
import com.ingenic.alarmclock.MainActivity;
import com.ingenic.alarmclock.R;
import com.ingenic.database.AlarmClockDbOperation;
import com.ingenic.database.AlarmClockValue;
import com.ingenic.iwds.utils.IwdsLog;

public class TimeDayAdapter extends ArrayAdapter<TimeDay> {

    private int mResourceId;

    private AlarmClockDbOperation mDbOperation;

    private Context mContext;

    private int mCount;
    private boolean mIsUpdate;
    private final int MSG_DB_UPDATE = 1;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_DB_UPDATE:
                // broadcast notice
                noticeReceiver();
                mIsUpdate = false;
                break;

            default:
                break;
            }
        };
    };

    public TimeDayAdapter(Context context, int resource, List<TimeDay> objects) {
        super(context, resource, objects);
        mResourceId = resource;
        mDbOperation = new AlarmClockDbOperation(context);
        mContext = context;
    }

	// transfer:"Sun Mon "->"11000000" "once "or"一次 " ->"00000001"
	private String transferString(String tempString) {
		int j = 0;
		String numString = "";
		int length = tempString.length();
		int datelength = mContext.getResources().getStringArray(
				R.array.day_week)[0].length();
		// "once "or"一次 " ->"00000001"
		if (length == 5 || length == 3) {
			numString = "00000001";
		} else {
			for (int i = 0; i < length;) {
				if ((tempString.substring(i, i + datelength)).equals(mContext
						.getResources().getStringArray(R.array.day_week)[j])) {
					numString += "1";
					i += datelength + 1;
				} else {
					numString += "0";
				}
				j++;
			}
			while (j < 8) {
				numString += "0";
				j++;
			}
		}
		return numString;
	}

    // transfer string to integer,like 02:20->2*60+20
    private int transferMinutes(String listminute) {
        return Integer.parseInt(listminute.substring(0, 2)) * CommonUtils.HOUR_TO_SECOND
                + Integer.parseInt(listminute.substring(3, 5));
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get the ListView's Item Data
        if (position > mCount - 1) {
            return convertView;
        }

        final TimeDay tempData = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            // first time get the layout and initialize
            convertView = LayoutInflater.from(getContext()).inflate(mResourceId,
                    null);
            viewHolder = new ViewHolder();
            viewHolder.listviewButton = (Button) convertView
                    .findViewById(R.id.listviewButton);
            viewHolder.timeTextView = (TextView) convertView
                    .findViewById(R.id.timeTextView);
            viewHolder.timeTextView.setTypeface(MainActivity.mTf);
            viewHolder.dayTextView = (TextView) convertView
                    .findViewById(R.id.dayTextView);
            viewHolder.dayTextView.setTypeface(MainActivity.mTf);
            viewHolder.itemLinear = (LinearLayout) convertView
                    .findViewById(R.id.itemLinear);
            convertView.setTag(viewHolder); // save ID
        } else {
            // second time get the before resource
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set TextView in ListView
        viewHolder.timeTextView.setText(tempData.getTimes());
         //把日 一 二 三 四 五 六 改显示为 每天/Everyday
        if(tempData.getDays().equals(mContext.getResources().getStringArray(
                R.array.day_week)[9]+" ")){
            viewHolder.dayTextView.setText(mContext.getResources().getStringArray(
                    R.array.day_week)[8]);
        }else{
            viewHolder.dayTextView.setText(tempData.getDays());
        }

        // set Button color
        if (tempData.isEnable()) {
            viewHolder.listviewButton.setBackgroundResource(R.drawable.btnopen);
        } else {
            viewHolder.listviewButton
                    .setBackgroundResource(R.drawable.btnclose);
        }

        final Button button = viewHolder.listviewButton;

        // monitor the item
        viewHolder.itemLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsUpdate) {
                    return;
                }
                mIsUpdate = true;
                if (tempData.isEnable()) {
                    button.setBackgroundResource(R.drawable.btnclose);
                    tempData.setEnable(false);
                } else {
                    button.setBackgroundResource(R.drawable.btnopen);
                    tempData.setEnable(true);
                }

                // updateDatabase(tempData);
                new UpdateAsyncTask().execute(tempData);
            }
        });
        return convertView;

    }

    class UpdateAsyncTask extends AsyncTask<TimeDay, Void, Void> {

        @Override
        protected Void doInBackground(TimeDay... params) {
            TimeDay tempData = params[0];
            mDbOperation
                    .update(new AlarmClockValue(tempData.getTimes(),
                            transferString(tempData.getDays()), 2, tempData
                                    .isEnable()));

            // notice the state is change,update table alarmclock
            updateTableAlarmClock(tempData);
            tempData = null;
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mHandler.obtainMessage(MSG_DB_UPDATE).sendToTarget();
        }

    }

    private void updateTableAlarmClock(TimeDay tempData) {
        String nowString = transferString(tempData.getDays());

        // when your turn on the clock,update the alarm clock
        if (tempData.isEnable()) {
            for (int i = 0; i < CommonUtils.DAY_WEEK; i++) {
                if (nowString.charAt(i) == '1') {
                    mDbOperation.update(new AlarmClockValue(
                            transferMinutes(tempData.getTimes()), i, 1, true));
                }
            }
            nowString = null;
            return;
        }

        // close the clock
        String openString = mDbOperation.querySecondState(new AlarmClockValue(
                tempData.getTimes(), transferString(tempData.getDays()), 2,
                true));

        for (int i = 0; i < CommonUtils.DAY_WEEK; i++) {
            if (nowString.charAt(i) == '1') {
                if (!openString.equals(CommonUtils.EMPTY_STRING)
                        && openString.charAt(i) == '1') {
                    // do nothing
                } else {
                    // update set the record false
                    mDbOperation.update(new AlarmClockValue(
                            transferMinutes(tempData.getTimes()), i, 1, false));
                }
            }
        }

    }

    private void noticeReceiver() {
        Intent intent = new Intent(mContext, ClockReceiver.class);
        intent.setAction(CommonUtils.ACTION_NOTICE);
        PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent,
                0);
        AlarmManager alarm = (AlarmManager) mContext
                .getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), sender);
    }

    private class ViewHolder {
        Button listviewButton;
        TextView timeTextView, dayTextView;
        LinearLayout itemLinear;
    }

}
