/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.utils;

import android.app.Activity;
import android.app.Application;

public class AlarmClockGlobal extends Application {
    private Activity mNumberPickerActivity;
    private long mTimeMillis;

    public long getTimeMillis() {
        return mTimeMillis;
    }

    public void setTimeMillis(long timeMillis) {
        this.mTimeMillis = timeMillis;
    }

    public Activity getNumberPickerActivity() {
        return mNumberPickerActivity;
    }

    public void setNumberPickerActivity(Activity numberPickerActivity) {
        this.mNumberPickerActivity = numberPickerActivity;
    }

}
