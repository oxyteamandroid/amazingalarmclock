/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.alarmclock;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.ingenic.database.AlarmClockDbOperation;
import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.utils.AlarmClockGlobal;
import com.ingenic.utils.CommonUtils;

import java.util.Calendar;

public class ClockReceiver extends BroadcastReceiver implements
        ServiceClient.ConnectionCallbacks {

    private boolean wait;
    // 广播ACTION
    private final String ACTION_ONCE = "com.ingenic.amazingalarmclock.once";
    private final String ACTION_BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";
    private final String ACTION_LAUNCHER_ACTION = "com.ingenic.getclock.data";
    private final String ACTION_CLEAN_DATA_OVER_ACTION = "ingenic.intent.action.com.ingenic.alarmclock";

    private ServiceClient mClient;
    private NotificationProxyServiceManager mService;

    private Context mContext;
    private Calendar mCalendar = null;

    private final int mId = 1;
    private final int mClearId = -1;

    // 用于操作数据库
    private AlarmClockDbOperation mDbOperation;
    // 保存下一个闹钟的秒数
    private static int[] mSeconds = new int[3];
    private static final long BUFFER_ALARM_TIME = 15 * 1000;

    @SuppressLint("HandlerLeak")
    private Handler mHandler2 = new Handler() {
        public void handleMessage(android.os.Message msg) {
            // 根据数据库的查询结果确定是要建立新的闹钟还是取消通知
            if (mSeconds[0] != -1) {
                sendOnce(mSeconds[0] * 1000);
            } else {
                cancelNotify();
            }
        }

        ;
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        if (mClient == null) {
            mClient = new ServiceClient(mContext,
                    ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
            mClient.connect();
        }
        // 获得数据库对象
        mDbOperation = new AlarmClockDbOperation(mContext);
        final String action = intent.getAction();
        if (ACTION_ONCE.equals(action)) {
            // 启动提示界面
            IwdsLog.d(ClockReceiver.this, "nowtime:" + getNowTimeMillis());
            IwdsLog.d(ClockReceiver.this, "gettime:" + getTimeMillis());
            IwdsLog.d(ClockReceiver.this, "distance:"
                    + (getNowTimeMillis() - getTimeMillis()));
            long distance = getNowTimeMillis() - getTimeMillis();
            if ((distance >= 0 && distance < BUFFER_ALARM_TIME)
                    || getTimeMillis() == 0) {
                Intent intentActivity = new Intent(context,
                        ClockInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(CommonUtils.BUNDLE_MINUTES, mSeconds[1]);
				  bundle.putInt(CommonUtils.BUNDLE_REPEATDAY, mSeconds[2]);
				  intentActivity.putExtras(bundle);
                intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentActivity);
                IwdsLog.d(ClockReceiver.this, "start activity to alarm");
            }

        } else if (ACTION_BOOT_ACTION.equals(action) || ACTION_CLEAN_DATA_OVER_ACTION.equals(action)) {
        } else if (CommonUtils.ACTION_NOTICE.equals(action)
                || Intent.ACTION_TIME_CHANGED.equals(action)
                || Intent.ACTION_TIMEZONE_CHANGED.equals(action)
                || ACTION_LAUNCHER_ACTION.equals(action)
                || ConnectionServiceManager.ACTION_CONNECTED_ADDRESS
                .equals(action)
                || ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS
                .equals(action)) {
            // 取消并重设闹钟
            cancelClockReceiver();
        }
        // 接到ACTION后都会执行如下操作:获得最近的闹钟
        setAlarm();
    }

    /**
     * 获得最近的闹钟
     */
    private void setAlarm() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                mSeconds = mDbOperation.queryFirstSeconds();
                mHandler2.obtainMessage().sendToTarget();
            }
        }).start();
    }

    /**
     * 取消通知
     */
    private void cancelNotify() {
        if (wait) {
            notifyStatusBar("", "");
        } else {
            mHandler.sendEmptyMessageDelayed(MAG_CANCEL_NOTIFY,
                    DELAYED_SEND_NOTIFY);
        }
    }

    /**
     * 发送闹钟给Receiver
     *
     * @param times
     */
    private void sendOnce(long times) {
        Intent intent = new Intent(mContext, ClockReceiver.class);
        intent.setAction(ACTION_ONCE);
        PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent,
                0);
        initCalendar();

        AlarmManager alarm = (AlarmManager) mContext
                .getSystemService(Context.ALARM_SERVICE);
        // RTC_WAKEUP使得接收者知道你是否修改了系统时间
        long millis = mCalendar.getTimeInMillis() + times;
        alarm.set(AlarmManager.RTC_WAKEUP, millis, sender);
        setTimeMillis(millis);
        // 发送通知
        mCalendar.add(Calendar.SECOND, (int) (times / 1000));
        String title = mContext.getResources().getString(R.string.alarmTitle);
        String temp = ":";
        if (mCalendar.get(Calendar.MINUTE) < 10) {
            temp = ":0";
        }
        String msg = mCalendar.get(Calendar.HOUR_OF_DAY) + temp
                + mCalendar.get(Calendar.MINUTE);
        if (wait) {
            notifyStatusBar(title, msg);
        } else {
            Message message = new Message();
            message.what = MAG_SEND_NOTIFY;
            message.obj = msg;
            mHandler.sendMessageDelayed(message, DELAYED_SEND_NOTIFY);
        }
    }

    /**
     * 等待通知服务连接后发送闹钟到通知栏的消息索引
     */
    private static final int MAG_SEND_NOTIFY = 0;
    /**
     * 等待通知服务连接后取消通知栏中的闹钟的消息索引
     */
    private static final int MAG_CANCEL_NOTIFY = 1;
    /**
     * 延时300毫秒发送消息
     */
    private static final int DELAYED_SEND_NOTIFY = 300;

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MAG_SEND_NOTIFY:
                    if (wait) {
                        String title = mContext.getResources().getString(
                                R.string.alarmTitle);
                        notifyStatusBar(title, (String) msg.obj);
                    } else {
                        mHandler.sendEmptyMessageDelayed(MAG_SEND_NOTIFY, DELAYED_SEND_NOTIFY);
                    }
                    break;
                case MAG_CANCEL_NOTIFY:
                    if (wait) {
                        notifyStatusBar("", "");
                    } else {
                        mHandler.sendEmptyMessageDelayed(MAG_CANCEL_NOTIFY,
                                DELAYED_SEND_NOTIFY);
                    }
                    break;
                default:
                    break;
            }
        }

        ;
    };

    private void notifyStatusBar(final String msg, final String content) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (mService != null) {
                    initCalendar();
                    if (msg.equals("") && content.equals("")) {
                        mService.notify(mClearId, new Note("Clear By Id", mId
                                + ""));
                    } else {
                        Note note = new Note(msg, content,
                                PendingIntent
                                        .getActivity(mContext, 0, new Intent(
                                                        mContext, MainActivity.class),
                                                0));
                        mService.notify(mId, note);
                    }
                    wait = false;
                }
            }
        }).start();
    }

    // cancel clock
    private void cancelClockReceiver() {
        Intent intent = new Intent(mContext, ClockReceiver.class);
        intent.setAction(ACTION_ONCE);
        PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent,
                0);
        AlarmManager alarm = (AlarmManager) mContext
                .getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(sender);
    }

    private void setTimeMillis(long timeMillis) {
        AlarmClockGlobal global = (AlarmClockGlobal) mContext
                .getApplicationContext();
        global.setTimeMillis(timeMillis);
    }

    private void initCalendar() {
        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }
        mCalendar.setTimeInMillis(System.currentTimeMillis());
    }

    private long getTimeMillis() {
        AlarmClockGlobal global = (AlarmClockGlobal) mContext
                .getApplicationContext();
        return global.getTimeMillis();
    }

    // 获取当前时间
    private long getNowTimeMillis() {
        return System.currentTimeMillis();
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mService = (NotificationProxyServiceManager) mClient
                .getServiceManagerContext();
        wait = true;
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
                                ConnectFailedReason reason) {

    }

}
