/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.alarmclock;

import java.util.Calendar;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.utils.AlarmClockGlobal;
import com.ingenic.utils.CommonUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.Formatter;
import android.widget.NumberPicker.OnValueChangeListener;

public class NumberPickerActivity extends RightScrollActivity implements
        OnClickListener {

    /**
     * 右滑控件
     */
    private RightScrollView mView;


    private NumberPicker mHourPicker;
    private NumberPicker mMinutePicker;
    /**
     * TimePicker的时和分
     */
    private int mHour;
    private int mMinute;

    private Button mCancelButton, mNextButton;

    private Calendar mCalendar = null;

    private OnValueChangeListener mOnValueChangedListener = new OnValueChangeListener() {

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            switch (picker.getId()) {
            case R.id.clock_hour_picker:
                mHour = newVal;
                break;
            case R.id.clock_minute_picker:
                mMinute = newVal;
                break;
            default:
                break;
            }
        }
    };

    private Formatter mFormatter = new Formatter() {

        @Override
        public String format(int value) {
            String tmpStr = String.valueOf(value);
            if (value < 10) {
                tmpStr = "0" + tmpStr;
            }
            return tmpStr;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = getRightScrollView();
        mView.disableRightScroll();
        mView.setContentView(R.layout.numberpicker);

        findViews();
        setViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IwdsLog.d(NumberPickerActivity.this, "NumberPickerActivity onResume");
        mView.enableRightScroll();
        buttonEnable(true);
    }

    private void findViews() {
        mHourPicker = (NumberPicker) findViewById(R.id.clock_hour_picker);
        mMinutePicker = (NumberPicker) findViewById(R.id.clock_minute_picker);
        mCancelButton = (Button) findViewById(R.id.cancelnumberButton);
        mNextButton = (Button) findViewById(R.id.nextButton);
    }

    @SuppressLint("UseValueOf")
    private void setViews() {
        initCalendar();

        initNumberPicker(mHourPicker, 23, 0, mCalendar.get(Calendar.HOUR_OF_DAY));
        initNumberPicker(mMinutePicker, 59, 0, mCalendar.get(Calendar.MINUTE));

        mCancelButton.setOnClickListener(NumberPickerActivity.this);
        mNextButton.setOnClickListener(NumberPickerActivity.this);
        buttonEnable(false);
        setNumberPickerActivity(NumberPickerActivity.this);
    }

    private void initNumberPicker(NumberPicker numberPicker, int maxValue, int minValue,
            int nowValue) {
        // 设置NumberPicker的显示格式，使得0-9显示为00-09
        numberPicker.setFormatter(mFormatter);
        numberPicker.setOnValueChangedListener(mOnValueChangedListener);
        numberPicker.setMaxValue(maxValue);
        numberPicker.setMinValue(minValue);
        numberPicker.setValue(nowValue);
        // 以下三句使得NumberPicker无法点击输入
        numberPicker
                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    public void onRightScroll() {
        finish();
    }

    private void setNumberPickerActivity(Activity activity) {
        AlarmClockGlobal app = (AlarmClockGlobal) getApplication();
        app.setNumberPickerActivity(activity);
    }

    private void buttonEnable(boolean enable) {
        mCancelButton.setClickable(enable);
        mNextButton.setClickable(enable);
    }

    private void initCalendar() {
        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }
    }

    @Override
    public void onClick(View v) {
        // button如果在指定时间内重复点击，则无效
        IwdsLog.d(NumberPickerActivity.this, "NumberPickerActivity onClick");
        buttonEnable(false);
        mView.disableRightScroll();
        switch (v.getId()) {
        case R.id.cancelnumberButton:
            finish();
            break;
        case R.id.nextButton:
            mHour = mHourPicker.getValue();
            mMinute = mMinutePicker.getValue();
            Intent intent = new Intent(NumberPickerActivity.this,
                    TimeDayActivity.class);
            intent.putExtra(CommonUtils.HOUR_KEY, mHour);
            intent.putExtra(CommonUtils.MINUTE_KEY, mMinute);
            startActivity(intent);
            break;
        default:
            break;
        }
    }

}
