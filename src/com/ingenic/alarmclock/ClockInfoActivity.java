/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.alarmclock;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.ingenic.database.AlarmClockDbOperation;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.utils.CommonUtils;
import com.ingenic.utils.RingInsist;
import com.ingenic.utils.ScreenListener;
import com.ingenic.utils.ScreenListener.ScreenStateListener;

public class ClockInfoActivity extends RightScrollActivity implements
        OnClickListener {
    // 闹铃开始和停止的广播ACTION
    private static final String ACTION_STOP = "com.ingenic.clock.stop";
    private static final String ACTION_START = "com.ingenic.clock.start";
    private static final String ACTION_COUNTDOWN = "com.ingenic.countdown.ring";
    private static final String ACTION_ALARMCLOCK = "com.ingenic.alarmclock.ring";
    private static final String ACTION_OUT_CALL = "com.ingenic.mobilecenter.action.OUT_CALL";
    private static final String ACTION_INCOMING_CALL = "com.ingenic.mobilecenter.action.INCOMING_CALL";

    private ScreenListener mScreenListener = null;
    private CountDownReceiver mCountDownReceiver = new CountDownReceiver();

    private Vibrator mVibrator = null;
    private AlarmClockDbOperation mDbOperation;
    private Button mConfiButton;

    private PowerManager.WakeLock mWakeLock = null;
    // 右滑控件
    private RightScrollView mView;

    private boolean mFlag = false;
    private int mRepeatDay, mMinutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sendBroadcast(new Intent(ACTION_ALARMCLOCK));
        mScreenListener = new ScreenListener(this);
        // the follow three sentences is need by right slide
        mView = getRightScrollView();
        mView.disableRightScroll();
        mView.setContentView(R.layout.clockinfo);

        mDbOperation = new AlarmClockDbOperation(this);
        Bundle bundle = getIntent().getExtras();
        mRepeatDay = bundle.getInt(CommonUtils.BUNDLE_REPEATDAY);
        mMinutes = bundle.getInt(CommonUtils.BUNDLE_MINUTES);
        TextView clockTime = (TextView) findViewById(R.id.clock_time);
        TextView am = (TextView) findViewById(R.id.clock_am);
        TextView pm = (TextView) findViewById(R.id.clock_pm);
        clockTime.setTypeface(MainActivity.mTf);
        am.setTypeface(MainActivity.mTf);
        pm.setTypeface(MainActivity.mTf);
        if ((mMinutes / 60) < 12) {
			 am.setVisibility(View.VISIBLE);
			 pm.setVisibility(View.GONE);
        } else {
			 am.setVisibility(View.GONE);
			 pm.setVisibility(View.VISIBLE);
         }
        clockTime.setText(CommonUtils.transferMinutes(mMinutes));

        // 一分钟后停止闹钟
        mHandler.sendEmptyMessageDelayed(STOP_CLOCK, 60 * 1000);

        // search ID
        findViews();

        // set control
        setViews();

        // start vibration
        vibratorStart();

        // 启动铃声
        ringStart();

        // wake up and light the screen
        wakeUpAndUnlock(ClockInfoActivity.this);
        mScreenListener.begin(new ScreenStateListener() {
            @Override
            public void onUserPresent() {
            }

            @Override
            public void onScreenOn() {
            }

            @Override
            public void onScreenOff() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 500);
            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_COUNTDOWN);
        filter.addAction(ACTION_OUT_CALL);
        filter.addAction(ACTION_INCOMING_CALL);
        registerReceiver(mCountDownReceiver, filter);

    }

    private static final int STOP_CLOCK = 0;
    private static final int UPDATELISTVIEW = 1;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case STOP_CLOCK:
                    // 关闭
                    onBackPressed();
                    break;
                case UPDATELISTVIEW:
				     // 发送广播给接收者
				       noticeReceiver();
				    // 更新ListView数据
				       sendBroadcast(new Intent(CommonUtils.ACTION_UPDATE_LISTVIEW));
				       break;

                default:
                    break;
            }

        }

        ;
    };

    private void noticeReceiver() {
		  Intent intent = new Intent(ClockInfoActivity.this, ClockReceiver.class);
		  intent.setAction(CommonUtils.ACTION_NOTICE);
		  PendingIntent sender = PendingIntent.getBroadcast(
				  ClockInfoActivity.this, 0, intent, 0);
		  AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
		  alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), sender);
	}

    private void findViews() {
        // button ID
        mConfiButton = (Button) findViewById(R.id.confirm_button);
    }

    private void setViews() {
        // monitor the button
        mConfiButton.setOnClickListener(ClockInfoActivity.this);
        mConfiButton.setClickable(false);
        // monitor right slide
        mView.setOnRightScrollListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.enableRightScroll();
        mConfiButton.setClickable(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // turn off light forever
        lightOff();

        // cancel vibration
        vibratorCancel();

        ringStop();

        if (mScreenListener != null) {
            mScreenListener.unregisterListener();
            mScreenListener = null;
        }
        mHandler.removeMessages(STOP_CLOCK);
        unregisterReceiver(mCountDownReceiver);
         // 从数据库中删除该一次性闹钟
		 if (mRepeatDay == 7) {
			 new Thread(new Runnable() {
				 @Override
				 public void run() {
					 // TODO Auto-generated method stub
					 mDbOperation.delete(mMinutes, mRepeatDay);
					 mHandler.obtainMessage(UPDATELISTVIEW).sendToTarget();
				 }
			 }).start();
		 }
    }

    private void ringStart() {
        Intent intent = new Intent(ACTION_START);
        sendBroadcast(intent);
        try {
            RingInsist.startRing(ClockInfoActivity.this);
        } catch (Exception e) {
            IwdsLog.e(this, e.getMessage());
        }
    }

    private void ringStop() {
        RingInsist.stopRing();
        // 表示是倒计时响铃导致的本此闹钟关闭，不必继续播放音乐
        if (mFlag) return;
        Intent intent = new Intent(ACTION_STOP);
        sendBroadcast(intent);
    }

    private void vibratorStart() {
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // stop start stop start
        long[] pattern = {500, 500, 500, 500};
        // repeat vibration follow the parent above
        mVibrator.vibrate(pattern, 2);
    }

    // unlock and light the screen
    @SuppressWarnings("deprecation")
    private void wakeUpAndUnlock(Context context) {
        KeyguardManager km = (KeyguardManager) context
                .getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");
        // unlock
        kl.disableKeyguard();
        // get the power manager object
        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        // get PowerManager.WakeLock object,the last parameter is tag in logcat
        mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.FULL_WAKE_LOCK, "bright");
        // light screen
        mWakeLock.acquire();

    }

    // turn off the screen light forever
    private void lightOff() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    private void vibratorCancel() {
        if (mVibrator != null) {
            mVibrator.cancel();
            mVibrator = null;
        }
    }

    @Override
    public void onClick(View v) {
        // button如果在指定时间内重复点击，则无效
        mConfiButton.setClickable(false);
        mView.disableRightScroll();
        switch (v.getId()) {
            case R.id.confirm_button:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    private class CountDownReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_COUNTDOWN.equals(action)
                    || ACTION_OUT_CALL.equals(action)
                    || ACTION_INCOMING_CALL.equals(action)) {
                mFlag = true;
                finish();
            }
        }
    }

}
