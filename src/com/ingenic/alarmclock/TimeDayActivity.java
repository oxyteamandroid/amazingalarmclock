/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.alarmclock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.ingenic.database.AlarmClockDbOperation;
import com.ingenic.database.AlarmClockValue;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.utils.AlarmClockGlobal;
import com.ingenic.utils.CommonUtils;
import com.ingenic.utils.DayState;

public class TimeDayActivity extends RightScrollActivity implements
        OnClickListener {
    // nine button include:once
    private static final int DAY_NUM =9;
    // use for right slide
    private RightScrollView mView;

    private Button mFinishButton, mCancelButton;
    /**
     * 日历
     */
    private Calendar mCalendar = null;

    // get the TimePicker's hour and minute
    private int mHour;
    private int mMinute;

    // database operation
    private AlarmClockDbOperation mDbOperation;

    // nine button
    private int[] mButtonId = { R.id.sunButton, R.id.monButton, R.id.tueButton,
            R.id.wedButton, R.id.thuButton, R.id.friButton, R.id.satButton,
            R.id.allButton ,R.id.onceButton };
	 // nine imageView
	 private int[] mImageViewId = { R.id.sunImageView, R.id.monImageView,
			R.id.tueImageView, R.id.wedImageView, R.id.thuImageView,
			R.id.friImageView, R.id.satImageView, R.id.allImageView,
			R.id.onceImageView };
    // sentence whether you click Button
    // private boolean isFirst = true;

    private List<DayState> dayList = new ArrayList<DayState>();

    private final int MSGFINISH = 1;
    private final int MSGTOAST = 2;

    private boolean mClickRepeat = true;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSGFINISH:
                // notice alarm clock
                noticeReceiver();
                onBackPressed();
                break;
            case MSGTOAST:
                AmazingToast.showToast(TimeDayActivity.this,
                        R.string.repeatInfo, AmazingToast.LENGTH_SHORT);
                break;
            default:
                break;
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // right slide need
        mView = getRightScrollView();
        mView.disableRightScroll();
        mView.setContentView(R.layout.timeday);

        Intent intent = getIntent();
        mHour = intent.getIntExtra(CommonUtils.HOUR_KEY, 0);
        mMinute = intent.getIntExtra(CommonUtils.MINUTE_KEY, 0);

        // get database object
        mDbOperation = new AlarmClockDbOperation(TimeDayActivity.this);

        findViews();

        setViews();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.enableRightScroll();
        buttonEnable(true);
    }

    private void findViews() {
        mFinishButton = (Button) findViewById(R.id.finishButton);
        mCancelButton = (Button) findViewById(R.id.cancelButton);
        buttonFindViews();

    }

    private void buttonFindViews() {
        for (int i = 0; i < DAY_NUM; i++) {
            dayList.add(new DayState((Button) findViewById(mButtonId[i]), false));
            dayList.get(i).getButton().setTextColor(Color.WHITE);
            dayList.get(i).getButton().setTypeface(MainActivity.mTf);
        }
    }

    private void buttonSetViews() {
        for (int i = 0; i < DAY_NUM; i++) {
            dayList.get(i).getButton().setOnClickListener(TimeDayActivity.this);
        }
    }

    private void setViews() {
        // monitor right slide
        // mView.setOnRightScrollListener(TimeDayActivity.this);

        mFinishButton.setOnClickListener(TimeDayActivity.this);
        mCancelButton.setOnClickListener(TimeDayActivity.this);
        buttonEnable(false);
        buttonSetViews();
    }

    private Activity getNumberPickerActivity() {
        AlarmClockGlobal app = (AlarmClockGlobal) getApplication();
        return app.getNumberPickerActivity();
    }


    private void noticeReceiver() {
        Intent intent = new Intent(TimeDayActivity.this, ClockReceiver.class);
        intent.setAction(CommonUtils.ACTION_NOTICE);
        PendingIntent sender = PendingIntent.getBroadcast(TimeDayActivity.this,
                0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), sender);
    }

    private void buttonEnable(boolean enable) {
        mCancelButton.setClickable(enable);
        mFinishButton.setClickable(enable);
//        mCancelButton.setEnabled(enable);
//        mFinishButton.setEnabled(enable);
    }

    class QueryAsyncTask extends AsyncTask<String, Void, Void> {
        private int flag = 0;

        @Override
        protected Void doInBackground(String... params) {
            String testString = params[0];
            if (mDbOperation.queryRepeat(new AlarmClockValue(CommonUtils.format(mHour)
                    + ":" + CommonUtils.format(mMinute), testString, 2, true))) {
                flag = 0;

            } else {
                mDbOperation.insert(new AlarmClockValue(CommonUtils.format(mHour) + ":"
                        + CommonUtils.format(mMinute), testString, 2, true));
                // not repeat,then operate table alarmclock
                // testString's length is 8
                for (int i = 0; i < DAY_NUM - 1; i++) {
                    if (testString.charAt(i) == '1') {
                        if (!mDbOperation.queryRepeat(new AlarmClockValue(mHour
                                * CommonUtils.HOUR_TO_SECOND + mMinute, i, 1, true))) {
                            mDbOperation.insert(new AlarmClockValue(mHour
                                    * CommonUtils.HOUR_TO_SECOND + mMinute, i, 1, true));
                        } else {
                            mDbOperation.update(new AlarmClockValue(mHour
                                    * CommonUtils.HOUR_TO_SECOND + mMinute, i, 1, true));
                        }
                    }
                }
                flag = 1;
            }
            testString = null;
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            switch (flag) {
            case 0:
                handler.removeMessages(MSGTOAST);
                handler.obtainMessage(MSGTOAST).sendToTarget();
                buttonEnable(true);
                mClickRepeat = true;
                break;
            case 1:
                handler.obtainMessage(MSGFINISH).sendToTarget();
                mClickRepeat = true;
                break;
            default:
                break;
            }
        }

    }

    private void initCalendar() {
        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }
        mCalendar.setTimeInMillis(System.currentTimeMillis());
    }

    @Override
    public void onClick(View v) {
        // button如果在指定时间内重复点击，则无效
        mView.disableRightScroll();
        buttonEnable(false);
        switch (v.getId()) {
        case R.id.cancelButton:
            // in TimePicker cancel
            onBackPressed();
            break;
        case R.id.finishButton:
			// clock is create finish
			// save data, operation database
			if (mClickRepeat) {
				mClickRepeat = false;
				if (getNumberPickerActivity() != null) {
					getNumberPickerActivity().finish();
				}
				String testString = "";
				// get the string means which button you press
				// 因为 DAY_NUM-2 对应的是"全部"的按键 所以跳过 保证最后得到的testString=“00000000”
				// 前7位对应的是日-六 最后一位对应的是 一次性闹钟
				for (int i = 0; i < DAY_NUM - 2; i++) {
					if (dayList.get(i).isSecond()) {
						testString += CommonUtils.REPEAT;
					} else {
						testString += CommonUtils.NOT_REPEAT;
					}
				}
				if (dayList.get(DAY_NUM - 1).isSecond()) {
					testString += CommonUtils.REPEAT;
				} else {
					testString += CommonUtils.NOT_REPEAT;
				}
				// 如果testString为空 没有按键被点击 则默认为当天
				if (testString.equals(CommonUtils.EMPTY_STRING)) {
					// not press button,get default value
					testString = "";
					initCalendar();
					int day = mCalendar.get(Calendar.DAY_OF_WEEK);
					for (int i = 1; i < DAY_NUM; i++) {
						if (i == day) {
							testString += CommonUtils.REPEAT;
						} else {
							testString += CommonUtils.NOT_REPEAT;
						}
					}
				}
				new QueryAsyncTask().execute(testString);
			}
            break;
		default:
			// 点击日期按钮时，允许点击取消和完成按钮
			buttonEnable(true);
			for (int i = 0; i < DAY_NUM; i++) {// 遍历所有按钮
				if (v.getId() == mButtonId[i]) {// 找到点击的按钮
					// 如果是最后一个按钮 一次
					if (i == DAY_NUM - 1) {
						if (dayList.get(i).isSecond()) {
							dayList.get(i).getButton()
									.setTextColor(Color.WHITE);
							findViewById(mImageViewId[i]).setVisibility(
									View.GONE);
						} else {
							dayList.get(i)
									.getButton()
									.setTextColor(
											getResources().getColor(
													R.color.purple));
							findViewById(mImageViewId[i]).setVisibility(
									View.VISIBLE);
							for (int k = 0; k < DAY_NUM - 1; k++) {
								dayList.get(k).getButton()
										.setTextColor(Color.WHITE);
								findViewById(mImageViewId[k]).setVisibility(
										View.GONE);
								dayList.get(k).setSecond(false);
							}
						}
						dayList.get(i).setSecond(!dayList.get(i).isSecond());
					} else if (i == DAY_NUM - 2) {// 是否是“全部（最后倒数第二个按钮）”按钮
						dayList.get(DAY_NUM - 1).getButton()
								.setTextColor(Color.WHITE);
						findViewById(mImageViewId[DAY_NUM - 1]).setVisibility(
								View.GONE);
						dayList.get(DAY_NUM - 1).setSecond(false);
						for (int j = 0; j < i + 1; j++) {
							if (dayList.get(i).isSecond()) {// 如果是取消全部
								dayList.get(j).getButton()
										.setTextColor(Color.WHITE);
								findViewById(mImageViewId[j]).setVisibility(
										View.GONE);
							} else {
								dayList.get(j)
										.getButton()
										.setTextColor(
												getResources().getColor(
														R.color.purple));
								findViewById(mImageViewId[j]).setVisibility(
										View.VISIBLE);
							}
							dayList.get(j)
									.setSecond(!dayList.get(i).isSecond());
						}
						// isFirst = false;
					} else {
						// press other button
						dayList.get(DAY_NUM - 1).getButton()
								.setTextColor(Color.WHITE);
						findViewById(mImageViewId[DAY_NUM - 1]).setVisibility(
								View.GONE);
						dayList.get(DAY_NUM - 1).setSecond(false);
						if (dayList.get(i).isSecond()) {
							dayList.get(i).getButton()
									.setTextColor(Color.WHITE);
							findViewById(mImageViewId[i]).setVisibility(
									View.GONE);
							dayList.get(DAY_NUM - 2).getButton()
									.setTextColor(Color.WHITE);
							findViewById(mImageViewId[DAY_NUM - 2])
									.setVisibility(View.GONE);
							dayList.get(DAY_NUM - 2).setSecond(false);
						} else {
							dayList.get(i)
									.getButton()
									.setTextColor(
											getResources().getColor(
													R.color.purple));
							findViewById(mImageViewId[i]).setVisibility(
									View.VISIBLE);

							boolean isSecond = true;
							// 遍历是否是全部选中
							for (int j = 0; j < dayList.size() - 2; j++) {
								if (j == i)
									continue;
								if (!dayList.get(j).isSecond()) {
									isSecond = false;
									break;
								}
							}
							if (isSecond) {// 如果是全部选中
								dayList.get(DAY_NUM - 2)
										.getButton()
										.setTextColor(
												getResources().getColor(
														R.color.purple));
								findViewById(mImageViewId[DAY_NUM - 2])
										.setVisibility(View.VISIBLE);
								dayList.get(DAY_NUM - 2).setSecond(true);
							}
						}
						dayList.get(i).setSecond(!dayList.get(i).isSecond());
					}
				}
			}
			break;
        }

    }

}
