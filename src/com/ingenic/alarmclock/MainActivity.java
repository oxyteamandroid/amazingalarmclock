/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingAlarmClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.alarmclock;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.ingenic.database.AlarmClockDbOperation;
import com.ingenic.database.AlarmClockValue;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AmazingSwipeListView;
import com.ingenic.iwds.widget.AmazingSwipeListView.OnItemDeleteListener;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.utils.CommonUtils;
import com.ingenic.utils.TimeDay;
import com.ingenic.utils.TimeDayAdapter;

public class MainActivity extends RightScrollActivity implements
        OnItemDeleteListener, OnClickListener {
    // 右滑
    private RightScrollView mView;
    // 添加闹钟
    private Button mAddButton;
    // 显示闹钟时间
    private List<TimeDay> mTimeDayLists = new ArrayList<TimeDay>();
    // 用于数据库操作的数据库对象
    private AlarmClockDbOperation mDbOperation;
    // 自定义ListView
    private AmazingSwipeListView mClockListView;
    // ListView标题
     private TextView mTitleTextView;
//    private ImageView mTitleImageView;
    // 适配器
    private TimeDayAdapter mClockAdapter;

    /**
     * mHandler的what
     */
    private static final int MSGUPDATE = 1;
    private static final int MSGRESUME = 2;
    private static final int MSGLISTVIEW = 3;
    private static final int MSGCLOCKNUM = 5;
    /**
     * 最多9个闹钟
     */
    private static final int CLOCK_NUM = 9;
    public static Typeface mTf; // 自定义字体

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSGUPDATE:
                mClockAdapter.notifyDataSetChanged();
                break;
            case MSGRESUME:
                mTimeDayLists.clear();
                @SuppressWarnings("unchecked")
                List<TimeDay> tempLists = (List<TimeDay>) msg.obj;
                int length = tempLists.size();
                for (int i = 0; i < length; i++) {
                    mTimeDayLists.add(tempLists.get(i));
                }
                revealDay();
                mClockListView.setAdapter(mClockAdapter);
                mHandler.obtainMessage(MSGUPDATE).sendToTarget();
                break;
            case MSGLISTVIEW:
                // 发送广播给接收者
                noticeReceiver();
                // 删除和更新ListView数据
                mTimeDayLists.remove(msg.arg1);
                mHandler.obtainMessage(MSGUPDATE).sendToTarget();
                break;
            case MSGCLOCKNUM:
                if (msg.arg1 >= CLOCK_NUM) {
                    AmazingToast.showToast(getApplicationContext(),
                            R.string.clock_too_much, AmazingToast.LENGTH_SHORT);
                    mView.enableRightScroll();
                    mAddButton.setClickable(true);
                    return;
                }
                Intent intent = new Intent(MainActivity.this,
                        NumberPickerActivity.class);
                startActivity(intent);
                break;
            default:
                break;
            }

        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 右滑需要语句，初始不允许右滑
        mView = getRightScrollView();

        mView.disableRightScroll();

        mView.setContentView(R.layout.alarmclock);

        // 获得数据库对象
        mDbOperation = new AlarmClockDbOperation(MainActivity.this);
        mTf = Typeface.createFromAsset(MainActivity.this.getAssets(),
				"fonts/fzlthjw.ttf");
        // 初始化控件
        findViews();
        // 初始化设置
        setViews();

        MainActivityReceiver receiver = new MainActivityReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(CommonUtils.ACTION_UPDATE_LISTVIEW);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mView != null) {
            mView.enableRightScroll();
        }
        // 允许创建闹钟
        mAddButton.setClickable(true);
        // 查询数据库获取数据
        new QueryAsyncTask().execute(0);
    }

    // change the reveal format,10000010->Sun Sat
    private void revealDay() {
        int length = mTimeDayLists.size();
        String resultString = "";
        // every ListView Item
        for (int i = 0; i < length; i++) {
            // every Bit
            for (int j = 0; j < CommonUtils.DAY_WEEK; j++) {
                if (mTimeDayLists.get(i).getDays().charAt(j) == CommonUtils.REPEAT) {
                    resultString += getResources().getStringArray(
                            R.array.day_week)[j];
                    resultString += " ";
                }
            }
            mTimeDayLists.get(i).setDays(resultString);
            resultString = "";
        }
        mClockAdapter.setCount(length);
        // release resource
        resultString = null;
    }

    private void findViews() {
        mClockListView = (AmazingSwipeListView) findViewById(R.id.alarmclock_listview);
//        mTitleImageView = new ImageView(MainActivity.this);
         mTitleTextView = new TextView(MainActivity.this);
        // mTitleTextView.setTypeface(mTf);//设置字体
        mAddButton = (Button) findViewById(R.id.addButton);
        mAddButton.setTypeface(mTf);

    }

    private void setViews() {
        // 设置标题
        mTitleTextView.setGravity(Gravity.CENTER);
        mTitleTextView.setTextAppearance(this,
                android.R.style.TextAppearance_Large);
        mTitleTextView.setText("");

//		 mTitleImageView.setImageResource(R.drawable.title_icon);
        mClockAdapter = new TimeDayAdapter(this, R.layout.item, mTimeDayLists);

        mClockListView.addHeaderView(mTitleTextView);
//        mClockListView.addHeaderView(mTitleImageView);
//        mClockListView.setDivider(getResources().getDrawable(android.R.drawable.divider_horizontal_dark));
        mClockListView.setDivider(new ColorDrawable(getResources().getColor(R.color.grey)));
        mClockListView.setDividerHeight(1);
        mClockListView.setHeaderDividersEnabled(false);
        mClockListView.setAdapter(mClockAdapter);

        // 删除监听
        mClockListView.setOnItemDeleteListener(MainActivity.this);

        mAddButton.setOnClickListener(MainActivity.this);
        mAddButton.setClickable(false);
    }

    private void noticeReceiver() {
        Intent intent = new Intent(MainActivity.this, ClockReceiver.class);
        intent.setAction(CommonUtils.ACTION_NOTICE);
        PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, 0,
                intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), sender);
    }

    @Override
    public void onDelete(View view, int position) {
        if (position < 0 || mTimeDayLists.size() == 0) {
            return;
        }

        new DeleteClockAsyncTask().execute(position);

    }

    class DeleteClockAsyncTask extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            int position = params[0];
            if (position >= mTimeDayLists.size() || position < 0) {
                return null;
            }

            String nowString = transferString(position);

            String openString = mDbOperation
                    .querySecondState(new AlarmClockValue(mTimeDayLists.get(
                            position).getTimes(), nowString, 2, true));

            String closeString = mDbOperation
                    .querySecondState(new AlarmClockValue(mTimeDayLists.get(
                            position).getTimes(), nowString, 2, false));

            for (int i = 0; i < CommonUtils.DAY_WEEK; i++) {
                if (nowString.charAt(i) == CommonUtils.REPEAT) {
                    if (!openString.equals(CommonUtils.EMPTY_STRING)
                            && openString.charAt(i) == CommonUtils.REPEAT) {
                        // 无需操作
                    } else if (!closeString.equals(CommonUtils.EMPTY_STRING)
                            && closeString.charAt(i) == CommonUtils.REPEAT) {
                        // 更新状态为false
                        mDbOperation.update(new AlarmClockValue(
                                transferMinutes(mTimeDayLists.get(position)
                                        .getTimes()), i, 1, false));
                    } else {
                        // 删除表1
                        mDbOperation.delete(new AlarmClockValue(
                                transferMinutes(mTimeDayLists.get(position)
                                        .getTimes()), i, 1, false));
                    }
                }
            }
            // 删除数据
            mDbOperation.delete(new AlarmClockValue(mTimeDayLists.get(position)
                    .getTimes(), transferString(position), 2, true));

            return position;
        }

        @Override
        protected void onPostExecute(Integer result) {
            IwdsLog.d("AlarmColck", "result = " + result + " handler = "
                    + mHandler);
            if (result == null) {
                return;
            }
            mHandler.obtainMessage(MSGLISTVIEW, result, 0).sendToTarget();
        }

    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, Integer> {
        private int count = 0;
        List<TimeDay> timeDayLists = null;

        @Override
        protected void onPostExecute(Integer result) {
            switch (result) {
            case 0:
                mHandler.obtainMessage(MSGRESUME, timeDayLists).sendToTarget();
                timeDayLists = null;
                break;
            case 1:
                mHandler.obtainMessage(MSGCLOCKNUM, count, 0).sendToTarget();
                break;
            default:
                break;
            }
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            switch (params[0]) {
            case 0:
                timeDayLists = new ArrayList<TimeDay>();
                mDbOperation.querySecond(timeDayLists);
                break;
            case 1:
                count = mDbOperation.querySecondCount();
                break;
            default:
                break;
            }
            return params[0];
        }

    }

    private int transferMinutes(String listminute) {
        return Integer.parseInt(listminute.substring(0, 2)) * CommonUtils.HOUR_TO_SECOND
                + Integer.parseInt(listminute.substring(3, 5));
    }

	// make the reveal format transfer to database format,like
	// "Sun Fri" -> "1000010" or "日 五"-> "10000100" "once "or"一次 " ->"00000001"
	private String transferString(int position) {
		String numString = "";
		if (mTimeDayLists.size() < position)
			return numString;

		int j = 0;
		int length = mTimeDayLists.get(position).getDays().length();
		int dateLength = getResources().getStringArray(R.array.day_week)[0]
				.length();

		// "once "or"一次 " ->"00000001"
		if (length == 5 || length == 3) {
			numString = "00000001";
		} else {
			for (int i = 0; i < length;) {
				if ((mTimeDayLists.get(position).getDays().substring(i, i
						+ dateLength)).equals(getResources().getStringArray(
						R.array.day_week)[j])) {
					numString += CommonUtils.REPEAT;
					i += dateLength + 1;
				} else {
					numString += CommonUtils.NOT_REPEAT;
				}
				j++;
			}
			while (j < CommonUtils.DAY_WEEK) {
				numString += CommonUtils.NOT_REPEAT;
				j++;
			}
		}
		return numString;
	}

    @Override
    public void onClick(View v) {
        // button如果在指定时间内重复点击，则无效
        mView.disableRightScroll();
        mAddButton.setClickable(false);
        new QueryAsyncTask().execute(1);

    }
    public class MainActivityReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (CommonUtils.ACTION_UPDATE_LISTVIEW.equals(intent.getAction())) {
				// 重新查询加载数据
				new QueryAsyncTask().execute(0);
			}
		}
	}
}
